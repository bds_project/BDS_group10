import pandas as pd
import geopandas as gpd
import random
from shapely.geometry import Point
from src.scripts.get_address import getAddressFromPoint
import os.path
from os import path
from definitions import RESOURCE_PATH, OUTPUT_PATH


def main():
    fillAddressFile(10)


def fillAddressFile(points_per_statsec):
    '''
    Create a address database with addresses per statistical sector
    :param points_per_statsec: amount of points within a statistical sector the address is wanted for
    :return: write csv with all address information to output/
    '''

    print("Info: Loading geojson file...")
    fname = os.path.join(RESOURCE_PATH, 'sh_statbel_statistical_sectors.geojson')

    outputfile = os.path.join(OUTPUT_PATH, 'address_sample.csv')
    if path.exists(outputfile):
        address_df = pd.read_csv(outputfile)
        print("loaded existing address database.")
    else:
        address_df = pd.DataFrame(
            columns=["CD_sector", "point_number", "municipality",
                     "zipcode", "street", "housenumber", "ID", "lat", "lon"])

    gdf = gpd.read_file(fname)
    gdf = gdf.to_crs(epsg=4326)

    count = 0
    print("Info: Adding addresses...")

    for i, row in gdf.iterrows():

        count += 1
        # does the statsec already contain addresses?
        stsc = address_df[address_df["CD_sector"] == row.CD_SECTOR]
        if not stsc.empty:
            # how many do we still need?
            points_to_generate = points_per_statsec - stsc.shape[0]
            if points_to_generate == 0:
                print("already collected addresses in", row.CD_SECTOR)
                continue
        else:
            points_to_generate = points_per_statsec

        p = generate_random(points_to_generate, row.geometry)

        for i, point in enumerate(p):
            addr = getAddressFromPoint(point.y, point.x)
            addr["CD_sector"] = row.CD_SECTOR
            addr["point_number"] = i + 1
            address_df = address_df.append(addr, ignore_index=True)
        if count % 10 == 0:
            address_df.to_csv(outputfile, index=False)

    print("Info: Finished all addresses")



def generate_random(number, polygon):
    '''
    Generate a number of Points (lat, lon) within a statistical sector
    :param number: amount of points to be generated
    :param polygon: shape of the statistical sector
    :return: list of Points within the polygon of type List()
    '''
    list_of_points = []
    minx, miny, maxx, maxy = polygon.bounds
    counter = 0
    while counter < number:
        pnt = Point(random.uniform(minx, maxx), random.uniform(miny, maxy))
        if polygon.contains(pnt):
            list_of_points.append(pnt)
            counter += 1
    return list_of_points


if __name__ == "__main__":
    main()
