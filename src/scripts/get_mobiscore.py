#!/usr/bin/python3

import time
from selenium import webdriver
from definitions import OUTPUT_PATH, RESOURCE_PATH
import os
from src.manage_data import get_address_data


def get_driver():
    '''
    Creates headless google chrome driver for Selenium scraping
    :return: google chrome driver
    '''
    # headless chrome (https://developers.google.com/web/updates/2017/04/headless-chrome)
    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    driver = webdriver.Chrome(chrome_options=options)
    return driver


def _get_mobiscore_for_address(row, driver):
    '''
    Scrape the mobiscore and partial mobiscores for a given address
    :param row: row containing address fields
    :param driver: google chrome driver
    :return: row extended with mobiscore fields
    '''
    score = 100  # default value if not overwritten
    pt = 100
    edu = 100
    comm = 100
    leisure = 100
    health = 100
    
    print(row.name)

    driver.get('https://mobiscore.omgeving.vlaanderen.be/')
    address_fields = driver.find_elements_by_class_name('form-control')

    # clear for safety
    for f in address_fields:
        f.clear()

    # fill in form
    try:
        address_fields[0].send_keys(row['streetname_nl'])
        address_fields[1].send_keys(row['housenumber'])
        address_fields[2].send_keys(row['municipality_nl'])
    except IndexError:
        print('Could not fill form. Returning default values')
        row['mobiscore'] = score
        row['public_transport'] = pt
        row['education'] = edu
        row['commercial'] = comm
        row['leisure'] = leisure
        row['healthcare'] = health

        return row

    # submit form, check if gdpr popup
    try:
        gdpr = driver.find_element_by_id('gdpr_modal_confirm_btn')
        gdpr.click()  # gdpr consent
    except:
        # no gdpr popup: continue
        pass

    submit = driver.find_element_by_id('js-address-submit-button')
    submit.click()

    # scrape score
    time.sleep(1)  # wait for page to load
    try:
        # myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.ID, 'IdOfMyElement')))
        spans = driver.find_elements_by_tag_name("span")
        score = spans[20].text
        pt = spans[29].text[-1:]
        edu = spans[31].text[-1:]
        comm = spans[33].text[-1:]
        leisure = spans[35].text[-1:]
        health = spans[37].text[-1:]
        
    except:
        print("could not read response")
        

    row['mobiscore'] = score
    row['public_transport'] = pt
    row['education'] = edu
    row['commercial'] = comm
    row['leisure'] = leisure
    row['healthcare'] = health
    print("Mobiscore: " + str(score) + ": " +str(pt) + " (pt), " + str(edu) + " (edu), " + str(comm)+ " (comm), " + str(leisure) + " (leisure), " + str(health)+ " (health)")
    
    return row


if __name__ == "__main__":
    driver = get_driver()
    samples = get_address_data()
    gewesten = ["BE.BRUSSELS.BRIC.ADM.ADDR","https://data.vlaanderen.be/id/adres/"]  # filter on Brussels, Flanders
    samples = samples[samples['best_namespace'].isin(gewesten)]
    try:
        samples = samples.apply(_get_mobiscore_for_address, args=(driver,), axis=1)
        samples.to_csv(os.path.join(OUTPUT_PATH, 'address_mobiscore_partial_scores.csv'), index=False)
        driver.close()
    except:
        driver.close()

