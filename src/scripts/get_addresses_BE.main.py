import geopandas as gpd
import pandas as pd

from shapely.geometry import MultiPolygon
from shapely.strtree import STRtree


def main():
    # fillOutAddresses(points_to_sample=10)
    fillOutCorportateAddressesAlt()


def fillOutCorportateAddressesAlt():
    '''
    Enriched KBO information of companies all over Belgium with their a statistical sector, write to database
    :return: csv database file with enriched information
    '''

    df1 = gpd.read_file('./resources/Vkbo/vkbo_geo1.geojson')
    df2 = gpd.read_file('./resources/Vkbo/vkbo_geo2.geojson')
    df3 = gpd.read_file('./resources/Vkbo/vkbo_geo3.geojson')

    gdf_corp = pd.concat([df1, df2, df3])

    gdf_corp = gdf_corp.to_crs(epsg=4326)

    print("Info: Loading statistical sectors...")
    fname = "./resources/sh_statbel_statistical_sectors.geojson"
    gdf_statsec = gpd.read_file(fname)
    gdf_statsec = gdf_statsec.to_crs(epsg=4326)

    outputfile = "./output/corporate_addresses_vl.csv"

    polys = []
    for _, row in gdf_statsec.iterrows():
        poly = MultiPolygon(polygons=row.geometry)
        poly.CD_SECTOR = row.CD_SECTOR
        polys.append(poly)

    s_tree = STRtree(polys)
    findAddressPoly.i = 0
    gdf_corp["CD_SECTOR"] = gdf_corp.apply(lambda x: findAddressPoly(
        s_tree, x.geometry), axis=1)

    gdf_corp.to_csv(outputfile)


def findAddressPoly(s_tree, point):
    '''
    Check to which statistical sector a given point belongs using the Sort-Tile-Recursive algorithm
    :param s_tree: tree of shapes that will be queried
    :param point: point to be checked
    :return: the statistical sector
    '''
    findAddressPoly.i += 1

    polys = s_tree.query(point)
    poly = checkIfInPolys(polys, point)
    if poly is not None:
        sector = poly.CD_SECTOR
    else:
        sector = "Not matched"
    print(findAddressPoly.i, sector)
    return sector


def checkIfInPolys(polys, point):
    for poly in polys:
        if poly.contains(point):
            return poly
    return None



if __name__ == "__main__":
    main()
