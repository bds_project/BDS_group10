import requests


def getAddressFromPoint(lat, lon):
    '''
    Get the address of a geolocation
    :param lat: latitude of point
    :param lon: longitude of point
    :return: address object of type dict() with keys such as municipality, zipcode, housenumber, street
    '''
    url = "http://loc.geopunt.be/v4/Location"
    # v2/Location?q={q}&latlon={latlon}&xy={xy}&capakey={capakey}&poi={poi}&c={c}

    latlonparam = '{lat},{lon}'.format(lat=str(lat), lon=str(lon))
    PARAMS = {"latlon": [lat, lon]}
    PARAMS = {"latlon": latlonparam}

    res = requests.get(url, params=PARAMS)

    data = res.json()
    # print(resources)
    if 'Message' in data:
        return {}

    addr = {
        "municipality": data["LocationResult"][0]["Municipality"],
        "zipcode": data["LocationResult"][0]["Zipcode"],
        "street": data["LocationResult"][0]["Thoroughfarename"],
        "housenumber": data["LocationResult"][0]["Housenumber"],
        "ID": data["LocationResult"][0]["ID"],
        "lat": data["LocationResult"][0]["Location"]["Lat_WGS84"],
        "lon": data["LocationResult"][0]["Location"]["Lon_WGS84"],
    }

    return addr
