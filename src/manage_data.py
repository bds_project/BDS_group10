import pandas as pd
import os
from definitions import OUTPUT_PATH, RESOURCE_PATH
from collections import Counter
import urllib.request
import json

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)


def get_address_data():
    df = pd.read_csv(os.path.join(OUTPUT_PATH, 'address_sample.csv'))
    df = df.drop(df.columns[:2], axis=1)
    return df


def get_address_mobiscore_data():
    df = pd.read_csv(os.path.join(OUTPUT_PATH, 'address_mobiscore.csv'))
    return df


def fetch_latest_covid_data():
    url = "https://epistat.sciensano.be/Data/COVID19BE_CASES_MUNI_CUM.csv"
    path = os.path.join(RESOURCE_PATH, 'COVID_cases_cum_municipality.csv')
    urllib.request.urlretrieve(url, path)  # download to resources
    df_covid = pd.read_csv(os.path.join(RESOURCE_PATH, 'COVID_cases_cum_municipality.csv'),
                          encoding='cp1252')  # windows 1252 encoding
    return df_covid


# redundant, alternative method in json
def fetch_latest_covid_data_json():
    url_json = "https://epistat.sciensano.be/Data/COVID19BE_CASES_MUNI_CUM.json"
    response = urllib.request.urlopen(url_json)
    output = response.read().decode('cp1252')
    data = json.loads(output)

    return data


def download_fiscal_statsec_data():
    url = "https://statbel.fgov.be/sites/default/files/files/opendata/arbeid%20per%20sector/TF_PSNL_INC_TAX_SECTOR.xlsx"
    path = os.path.join(RESOURCE_PATH, 'fiscal_income_statsec.xlsx')
    urllib.request.urlretrieve(url, path)  # download to resources


def parse_fiscal_statsec_data():
    df_fiscal = pd.read_excel(os.path.join(RESOURCE_PATH, 'fiscal_income_statsec.xlsx'))
    #print(df_fiscal.tail())
    df_fiscal = df_fiscal[df_fiscal['CD_YEAR'] == 2017]  # filter on 2017
    df_fiscal.to_csv(os.path.join(RESOURCE_PATH, 'fiscal_income_statsec.csv'), index=False)
    os.remove(os.path.join(RESOURCE_PATH, 'fiscal_income_statsec.xlsx'))


def get_fiscal_statsec_data():
    df_fiscal = pd.read_csv(os.path.join(RESOURCE_PATH, 'fiscal_income_statsec.csv'))
    return df_fiscal


def download_population_statsec_data():
    url = "https://statbel.fgov.be/sites/default/files/files/opendata/bevolking/sectoren/OPEN%20DATA_SECTOREN_2019.xlsx"
    path = os.path.join(RESOURCE_PATH, 'population_statsec.xlsx')
    urllib.request.urlretrieve(url, path)  # download to resources


def parse_population_statsec_data():
    df_population = pd.read_excel(os.path.join(RESOURCE_PATH, 'population_statsec.xlsx'))
    print(len(df_population))
    df_population.to_csv(os.path.join(RESOURCE_PATH, 'population_statsec.csv'), index=False)
    os.remove(os.path.join(RESOURCE_PATH, 'population_statsec.xlsx'))


def get_population_statsec_data():
    df_population = pd.read_csv(os.path.join(RESOURCE_PATH, 'population_statsec.csv'))
    return df_population