import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is the Project Root
RESOURCE_PATH = os.path.join(ROOT_DIR, 'resources')
OUTPUT_PATH = os.path.join(ROOT_DIR, 'output')
