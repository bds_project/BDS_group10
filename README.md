# BDS group 10

This project is the back-end of the final assignment of Big Data Science (E018210A) of Timo Latruwe and Thibault Degrande.

## Goal of the project
Goal was to formulate a research or business question related to the COVID-19 pandemic, and turn a solution into a small product.

## Interpretation of the task
In the project, we gathered data on the statistical sectors in Flanders, Belgium, and checked if there is a correlation between them as well as with the numbers on reported COVID cases per municipality.
In particular, we investigated the correlation between those data sources and the mobiscore.
In particular, following data was obtained from open data sources available:
 * Car ownership
 * House sizes
 * Mobiscores
 * COVID-cases
 * Fiscal income
 * Business addresses
 * Population density

## Contents
In this gitlab project, the source code can be found that was used to complete the project.
* *src* hosts the scripts that have been used in preparing, fetching and processing the data, as well as notebooks that prepared and combined the data further to run our regression analysis. Finally, the pyspark_regression file contains the regression analysis.
* *resources* contains the data for each of the parameters
* *output* contains (pre)-processed data, as well as regression results

More information on this project and the results coming from this project can be found on the online documentation [webtool](http://54.93.238.98:8838/).
The source code to the tool is available [here](https://gitlab.com/bds_project/bds_dash).

![](resources/snippet.png)
